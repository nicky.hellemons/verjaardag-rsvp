import React from 'react';
import partypopper from './partypopper.gif';
import './App.css';
import { Button, FormControl, FormControlLabel, Radio, RadioGroup, TextField } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';




function App() {
  
  return (

    <div className="App">
      <body className="App-body">
        <div className="confetti-wrap">
          <h1 className="h1feestje">
            Feestje! 
          </h1>
          <img className="partypopper" src={partypopper} />
          <h3>Noor en Nicky zijn jarig, <br/> 
          dus vieren ze feest.</h3>
          <h5>📍 : KNWGBCAFE <br/>
          🗓 : 4 Juni 2022 <br/>
          ⌚️ : 20:00 <br />
          Kadotip : 💰 voor🍺
          </h5>
          <h4>Laat je even weten of je komt? <br />
          </h4>
          <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScTmkv9RQ2HpHpN5kbuCGNVeXwh5Y_HU3Ch5MnwELQ-nFfaeA/viewform?embedded=true" width="375" height="850" frameborder="0" marginheight="0" marginwidth="0">Laden…</iframe>
        </div>
      </body>
    </div>
  );
}

export default App;
